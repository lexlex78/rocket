var app = angular.module('app', [
//    'mgcrea.ngStrap',
    'ui-rangeSlider', 'minicolors', 'anglepicker', 'ngDialog', 'uiSwitch']);

app.config(function(minicolorsProvider) {
    angular.extend(minicolorsProvider.defaults, {
        inline: true
    });
});

    app.directive('invertval', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelController) {
            ngModelController.$parsers.push(function(data) {
                //convert data from view format to model format
                if (data > 100)data = 100;
                if (data < 0)data = 0;
                data = 100 - data;
                return data; //converted
            });

            ngModelController.$formatters.push(function(data) {
                //convert data from model format to view format
                if (data > 100)data = 100;
                if (data < 0)data = 0;
                data = 100 - data;
                return data; //converted
            });
        }
    }
});


app.config(['ngDialogProvider', function(ngDialogProvider) {
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-default',
            plain: false,
            showClose: true,
            controller: 'MainCtrl',
            closeByDocument: true,
            closeByEscape: true,
            closeByNavigation: false,
            appendTo: '#popup',
            preCloseCallback: false,
            overlay: false,
            cache: false
        });
    }]);


app.config(function($provide) {

    // Suffix is "Directive" for decorating directives.
    // $delegate refers to the original directive definition.
    $provide.decorator('ngDialogDirective', function($delegate, ngDialog) {

        // When decorating a directive, $delegate is an array with the
        // actual directive at the first index.
        var directive = $delegate[0];



        // Store a reference to the original directive linking function.
        var link = directive.link;

        // Hook into the compile phase of the directive.
        directive.compile = function() {

            // The function returned by compile is your new link function.
            // Or 'postLink', to be exact.
            return function(scope, el, attrs) {


                // Run the original link function.
                // link.apply(this, arguments);
                el.on('click', function(e) {
                    e.preventDefault();

                    var ngDialogScope = angular.isDefined(scope.ngDialogScope) ? scope.ngDialogScope : 'noScope';
                    angular.isDefined(attrs.ngDialogClosePrevious) && ngDialog.close(attrs.ngDialogClosePrevious);

                    var defaults = ngDialog.getDefaults();

                    ngDialog.open({
                        template: attrs.ngDialog,
                        className: attrs.ngDialogClass || defaults.className,
                        controller: attrs.ngDialogController || 'MainCtrl',
                        scope: ngDialogScope,
                        data: attrs.ngDialogData,
                        showClose: attrs.ngDialogShowClose === 'false' ? false : (attrs.ngDialogShowClose === 'true' ? true : defaults.showClose),
                        closeByDocument: attrs.ngDialogCloseByDocument === 'false' ? false : (attrs.ngDialogCloseByDocument === 'true' ? true : defaults.closeByDocument),
                        closeByEscape: attrs.ngDialogCloseByEscape === 'false' ? false : (attrs.ngDialogCloseByEscape === 'true' ? true : defaults.closeByEscape),
                        preCloseCallback: attrs.ngDialogPreCloseCallback || defaults.preCloseCallback,
                    });
                });
                ;
            };
        };

        // Return the decorated original ($delegate).
        return $delegate;
    });


});


app.controller('MainCtrl', function($scope, $rootScope, ngDialog) {


    $scope.$watchCollection('models', function(newVal, oldVal) {

        if (newVal != undefined && oldVal != undefined && newVal != oldVal) {
            if ($rootScope.globalData.editButton != 1) {
                $('.globalEditButton').addClass('fadeInDown').removeClass('fadeOutDown');
                $rootScope.globalData.editButton = 1;
            }
        }

    });


    $rootScope.$on('ngDialog.opened', function(e, $dialog) {


        var level = $dialog.find('[level]').attr('level');
        if (level != 1)
            level = 2;

        $('.edit-menu .txt').hide();


        if (level == 1) {
            var dialogs = $('.ngdialog').not('#' + $dialog.attr('id'));
            angular.forEach(dialogs, function(dialog) {
                ngDialog.close($(dialog).attr('id'), '');
            });
        }

        if (level == 2) {
            var dialogs = $('.ng-dialog-level2').not('#' + $dialog.attr('id'));
            angular.forEach(dialogs, function(dialog) {
                ngDialog.close($(dialog).attr('id'), '');
            });
        }

        $dialog.addClass('ng-dialog-level' + level);
        if (level > 1)
            $('.ng-dialog-level' + (level - 1)).addClass('ng-dialog-disabled');

        var win = $('#' + $dialog.attr('id'));
        var select_menu = $('.edit-menu .active');
        var center = select_menu.offset().top - (select_menu.height() / 2);
        var y = center - ((win.height() - 42) / 2);
        win.offset({top: y});

    });
    $rootScope.$on('ngDialog.closed', function(e, $dialog) {

        var level = $dialog.find('[level]').attr('level');
        if (level != 1)
            level = 2;

        if (level == 1) {
            var dialogs = $('.ng-dialog-level2');

            angular.forEach(dialogs, function(dialog) {
                ngDialog.close($(dialog).attr('id'), '');
            });
        }

        $('.globalEditButton').addClass('fadeOutDown').removeClass('fadeInDown');
        $rootScope.globalData.editButton = 0;// $rootScope.$apply();


        if ($('.ng-dialog-level2').length == 0)
            $('.ng-dialog-level' + (level - 1)).removeClass('ng-dialog-disabled');

    });
    $rootScope.$on('ngDialog.closing', function(e, $dialog) {

        if ($(".ngdialog").length == 1) {
            $('.edit-menu li').removeClass('active');
            $('.edit-menu .txt').show();
        }
    });

    $scope.editRootOn = function() {

        return $scope.editOn || $scope.editOn_;

    }


    $scope.switch = function(value, title, template, level) {
        if (value)
            ngDialog.open({template: template, level: level, title: title, controller: 'MainCtrl'});
    };

    $scope.hoverIn = function() {
        if (!$scope.editOn)
            $scope.hoverEdit = true;

    };

    $scope.hoverOut = function() {
        if (!$scope.editOn)
            $scope.hoverEdit = false;

    };

    $scope.hoverIn_ = function() {
        if (!$scope.editOn_)
            $scope.hoverEdit_ = true;

    };

    $scope.hoverOut_ = function() {
        if (!$scope.editOn_)
            $scope.hoverEdit_ = false;

    };

    $scope.editClick = function() {

        if ($scope.editOn == 1)
            ngDialog.closeAll();
        $scope.editOn = !$scope.editOn;
        $scope.editOn_ = 0;
        $scope.hoverEdit = !$scope.hoverEdit;
        ngDialog.closeAll();


    }

    $scope.editClick_ = function() {

        if ($scope.editOn_ == 1)
            ngDialog.closeAll();

        $scope.editOn_ = !$scope.editOn_;
        $scope.editOn = 0;
        $scope.hoverEdit_ = !$scope.hoverEdit_;


    }

    $scope.editMenuClick = function(e) {
        var item = $(e.currentTarget);
        item.parent().find('li').removeClass('active');
        item.addClass('active');
    }

    $scope.toggleActiv = function(e) {
        var item = $(e.currentTarget);
        item.toggleClass('active');
    }

    $scope.incVal = function(z) {
        return ++z;
    }
    $scope.decVal = function(z) {
        return --z;
    }

    //checkout_block
    $scope.checkout_block_limit = {min : 25, max : 75}
    $scope.checkout_blockA = function(z) {
        if (z<$scope.checkout_block_limit.min)z=$scope.checkout_block_limit.min;
        if (z>$scope.checkout_block_limit.max)z=$scope.checkout_block_limit.max;
        return z;
    }
    $scope.checkout_blockB = function(z) {
        z=(100-$scope.checkout_blockA(z))/2
        if (z<((100-$scope.checkout_block_limit.max)/2))z=($scope.checkout_block_limit.min/2);
        return z;
    }



});


app.directive('integer', function() {
    return {
        scope: {
            max: "=",
            min: "="
        },
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function(inputValue) {

                var transformedInput = '';
                transformedInput = inputValue.toString();
                transformedInput = transformedInput.replace(/[^0-9]/ig, '');

                if (transformedInput.length < 1)
                    transformedInput = 0;



                if (inputValue > scope.max) {
                    transformedInput = scope.max
                }
                if (inputValue < scope.min) {
                    transformedInput = scope.min
                }

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });

            modelCtrl.$formatters.push(function(inputValue) {
                //convert data from model format to view format
                var transformedInput = '';
                transformedInput = inputValue.toString();
                transformedInput = transformedInput.replace(/[^0-9]/ig, '');

                if (transformedInput.length < 1)
                    transformedInput = 0;



                if (inputValue > scope.max) {
                    transformedInput = scope.max
                }
                if (inputValue < scope.min) {
                    transformedInput = scope.min
                }

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });


        }
    };
});

app.run(function($rootScope) {
    $rootScope.globalData = {editButton: 0}
})

