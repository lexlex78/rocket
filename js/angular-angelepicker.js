
angular.module('anglepicker', [])
  .directive('anglepicker', function() {
    return {
        scope: {
            angle: "="
        },
        link: function(scope, elm, attrs) {

            elm.anglepicker({
                start: function(e, ui) {

                },
                change: function(e, ui) {
                    scope.angle=ui.value;

                },
                stop: function(e, ui) {
                    scope.$apply();
                },
                value: scope.angle
            });

            scope.$watch('angle', function(angle) {
                elm.anglepicker("value", angle);

            });

        }

    };
});